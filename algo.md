# Fumeur

## Allumer
### Entrée
    N: entier
    proba: reel
    set_allumette_droite: entier
    set_allumette_gauche: entier
### Variables
    nbAllumettesRestanteDroite: entier
    nbAllumettesRestanteGauche: entier
    moyenneDroite: entier
    moyenneGauche: entier
    d: entier
    g: entier
    cpt: entier
    poche: chaine de charactères
### Precondition
    d > 0 ou g > 0
    cpt < N
### Initialisation
    nbAllumettesRestanteDroite = 0
    nbAllumettesRestanteGauche = 0
    moyenneDroite = 0
    moyenneGauche = 0
    cpt = 0
### Réalisation
    Tant que cpt < N
        d = set_allumette_droite
        g = set_allumette_gauche
        Tant que g >= 0 et d >= 0
            poche = choisirPoche(proba)
            Si poche est égale à gauche alors
                Si g est égale à 0 alors
                    nbAllumettesRestanteDroite += d
                    sortir
                Sinon
                    g --
                Fin si
            Sinon si poche est égale à droite alors
                Si d est égale à 0 alors
                    nbAllumettesRestanteGauche += g
                    sortir
                Sinon
                    d --
                Fin si
            Fin si
        Fin tant que

        cpt ++
    Fin tant que

    moyenneDroite = calculMoyenne(N, nbAllumettesRestanteDroite)
    moyenneGauche = calculMoyenne(N, nbAllumettesRestanteGauche)
### Postcondition
    d est égale 0 ou g est égale 0
    cpt est égale N
### Sortie
    afficher moyenneDroite
    afficher moyenneGauche
<br/>

## choisirPoche
### Entrée
    proba: réél
### Variables
    poche: chaîne de caractère
    r: Roulette
### Invariant
    0,5 < proba < 1,5
### Initialisation
    r.proba = proba
    initialiser (r)
### Réalisation
    Si item(r) est égale 1 alors
        poche = gauche
    Sinon
        poche = droite
    Fin si
### Postcondition
### Sortie
    poche

<br/>

## calculMoyenne
### Entrée
    N: entier
    alumettesRestante: entier
### Variables
    p: entier
### Initialisation
    P = 0
### Réalisation
    p = alumettesRestante / N
### Sortie
    p