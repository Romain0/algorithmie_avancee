#!/usr/bin/env python
import argparse
import sys
from Allumette import Allumette

parser = argparse.ArgumentParser()

# arguments possibles
parser.add_argument('-t', '--tour', help='nombre de tour (defaut 10)', nargs='?', const=10, default=10, type=int)
parser.add_argument('-d', '--droite', help='nombre allumette droite (defaut 8)', nargs='?', const=8, default=8, type=int)
parser.add_argument('-g', '--gauche', help='nombre allumette gauche (defaut 10)', nargs='?', const=10, default=10, type=int)
parser.add_argument('-p', '--proba', help='probabilite de tirer à droite (defaut 0,5)', nargs='?', const=0.5, default=0.5, type=float)

# récupère les arguments
args = parser.parse_args()

if args.droite <= 0 and args.gauche <= 0:
    sys.exit('Erreur: Les deux boites d\'allumettes ne peuvent pas etre vide.')

allumette:Allumette = Allumette(args.tour, args.droite, args.gauche, args.proba)
allumette.allumer()