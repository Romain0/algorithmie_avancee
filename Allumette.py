import random

class Allumette:
    DROITE = "droite"
    GAUCHE = "gauche"

    def __init__(self, set_tirage:int, set_allumette_droite:int, set_allumette_gauche:int, set_proba:float) -> None:
        self.nb_tirage: int = set_tirage
        self.set_allumette_droite: int = set_allumette_droite
        self.set_allumette_gauche: int = set_allumette_gauche
        self.proba: float = set_proba
    
    def allumer(self) -> None:
        # declaration des variables
        nb_allumette_restante_droite: int = 0
        nb_allumette_restante_gauche: int = 0
        moyenne_droite: int = 0
        moyenne_gauche: int = 0
        cpt: int = 0

        while cpt < self.nb_tirage:
            # initialisation du nombre d'allumettes
            nb_allumette_droite: int = self.set_allumette_droite
            nb_allumette_gauche: int = self.set_allumette_droite

            while nb_allumette_droite >= 0 and nb_allumette_gauche >= 0:
                poche: str = ""
                poche = self.choisir_poche(self.proba) # 0.75 permet de privilégier 1/4 de plus la poche droite
                # diminution du nombre d'allumettes
                if poche == self.DROITE:
                    if nb_allumette_droite <= 0:
                        # ajout des valeurs en vue de calculer la moyenne
                        nb_allumette_restante_gauche += nb_allumette_gauche
                        break
                    else:
                        nb_allumette_droite -= 1
                elif poche == self.GAUCHE:
                    if nb_allumette_gauche <= 0:
                        nb_allumette_restante_droite += nb_allumette_droite
                        break
                    else:
                        nb_allumette_gauche -= 1

            cpt += 1

        moyenne_droite = self.calcul_moyenne(self.nb_tirage, nb_allumette_restante_droite)
        moyenne_gauche = self.calcul_moyenne(self.nb_tirage, nb_allumette_restante_gauche)

        # affichage des moyennes
        print("En moyenne, pour " + str(self.nb_tirage) + " tirage.")
        print("Il reste, " + str(moyenne_droite) + " allumettes a droite quand la boite de gauche est vide.")
        print("Il reste, " + str(moyenne_gauche) + " allumettes a gauche quand la boite de droite est vide.")

    # générateur du choix de la poche
    def choisir_poche(self, tronque: float = 0.5) -> str:
        poche:str
        nb_alea:float = random.randint(1, 99) / 100
        # interval par defaut ]0,5; 1,5[
        nb = nb_alea + tronque

        if int(nb) == 1:
            poche = self.GAUCHE
        else:
            poche = self.DROITE

        return poche

    # calcul de la moyenne
    # (nb_1 + nb_2 + ... + nb_n) / n
    def calcul_moyenne(self, nb_tirage: int, allumette_restante) -> int:
        p: int = 0
        p = allumette_restante / nb_tirage

        return p