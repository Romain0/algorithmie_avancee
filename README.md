# TP Noté

## Prérequis

### Télécharger python
<a href="https://www.python.org/downloads/">Télécharger pour Windows / Linux</a>

## Rendu

* Algorithmie
    - algo.md

* Code
    - main.py
    - Allumette.py

## Lancer le script
    py main.py [-h] [-t [TOUR]] [-d [DROITE]] [-g [GAUCHE]] [-p [PROBA]]
    options:
        -h, --help                      aide
        -t [TOUR], --tour [TOUR]        nombre de tour (defaut 10)
        -d [DROITE], --droite [DROITE]  nombre allumette droite (defaut 8)
        -g [GAUCHE], --gauche [GAUCHE]  nombre allumette gauche (defaut 10)
        -p [PROBA], --proba [PROBA]     probabilite de tirer à droite (defaut 0,5)

### Schéma du TP
<img src="./schemaTpNote.png" alt="schema TP">